#!/usr/bin/env python3

#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Copyright (C) 2018-2020
#    Laboratory of Systems Biology, Department of Cybernetics,
#    School of Science, Tallinn University of Technology
#   This file is part of project: IOCBIO Deconvolve



import numpy as np
cimport numpy as np
cimport cython

DTYPE = np.float64
ctypedef np.float64_t DTYPE_t

FTYPE = np.float32
ctypedef np.float32_t FTYPE_t

from libcpp.vector cimport vector
from libcpp cimport bool


cdef extern from "deconvolve.hpp" namespace "deconvolve":
    ctypedef int (*callbackfunc)(void *f, size_t iteration_number,
                                 double cmin, double cmax, double csum,
                                 double nrm2_prev, double nrm2_prevprev,
                                 double lmbda, double lambda_factor, double snr, const void *image)
    cdef cppclass Deconvolve[T]:
        Deconvolve() except +
        void set_psf(const vector[T] &data, size_t n1, size_t n2, size_t n3, T v1, T v2, T v3)
        void set_callback(callbackfunc cb, void *user_data, bool update_image)
        void clear_callback()
        void enable_regularization();
        void disable_regularization();
        void set_snr(T snr)
        void clear_snr()
        void set_lambda(T l)
        void clear_lambda()
        void set_max_iterations(size_t iters)
        void clear_max_iterations()
        int regularized()
        vector[T] convolve(const vector[T] &data, size_t n1, size_t n2, size_t n3, T v1, T v2, T v3)
        vector[T] deconvolve(const vector[T] &data, size_t n1, size_t n2, size_t n3, T v1, T v2, T v3)

cdef class CallBack:
    cdef object user_callback
    cdef bool with_image
    cdef object dtype
    cdef object shape
    def __init__(self, callback, with_image, data):
        self.user_callback = callback
        self.with_image = with_image
        self.dtype = data.dtype
        self.shape = data.shape
    cdef np.ndarray image(self, const void *image):
        cdef size_t sz
        cdef const double *pd = <const double*>image
        cdef const float *pf = <const float*>image

        if self.with_image:
            sz = self.shape[0]
            data = np.ndarray(self.shape, self.dtype)
            if self.dtype == np.float32:
                for i in range(sz):
                    data[i] = pf[i]
            elif self.dtype == np.float64:
                for i in range(sz):
                    data[i] = pd[i]
            else:
                print('Unknown type')
            return data
        return np.array((0))
    def callback(self, **kw):
        return (<object>self.user_callback)(**kw)


cdef int callback_for_deconvolution(void *f, size_t iteration_number,
                                    double cmin, double cmax, double csum,
                                    double nrm2_prev, double nrm2_prevprev,
                                    double lmbda, double lambda_factor, double snr, const void *image):
     return (<CallBack>f).callback(iteration_number=iteration_number, cmin=cmin, cmax=cmax, csum=csum,
                                 nrm2_prev=nrm2_prev, nrm2_prevprev=nrm2_prevprev, lmbda=lmbda, lmbda_factor=lambda_factor, snr=snr,
                                 image=(<CallBack>f).image(image))

cdef class PyDeconvolve:
    cdef Deconvolve[double] *thisptr # hold a C++ instance which we're wrapping

    def __cinit__(self):
        '''
        Parameters:
        -----------

        '''
        self.thisptr = new Deconvolve[double]()

    def __dealloc__(self):
        del self.thisptr

    def enable_regularization(self):
        self.thisptr.enable_regularization()

    def disable_regularization(self):
        self.thisptr.disable_regularization()

    def regularized(self):
        return self.thisptr.regularized()

    def set_snr(self, v):
        self.thisptr.set_snr(v)

    def clear_snr(self):
        self.thisptr.clear_snr()

    def set_lambda(self, v):
        self.thisptr.set_lambda(v)

    def clear_lambda(self):
        self.thisptr.clear_lambda()

    def set_max_iterations(self, v):
        self.thisptr.set_max_iterations(v)

    def clear_max_iterations(self):
        self.thisptr.clear_max_iterations()

    cpdef void set_psf(self, np.ndarray[DTYPE_t, ndim=1, mode="c"] data, size_t n1, size_t n2, size_t n3, double v1, double v2, double v3):
        '''
        Parameters:
        -----------

        '''
        self.thisptr.set_psf(data, n1, n2, n3, v1, v2, v3)

    cpdef vector[double] convolve(self, np.ndarray[DTYPE_t, ndim=1, mode="c"] data, size_t n1, size_t n2, size_t n3, double v1, double v2, double v3):
        '''
        Parameters:
        -----------

        '''
        return self.thisptr.convolve(data, n1, n2, n3, v1, v2, v3)

    cpdef vector[double] deconvolve(self, np.ndarray[DTYPE_t, ndim=1, mode="c"] data, size_t n1, size_t n2, size_t n3, double v1, double v2, double v3, callback=None, callback_with_image=False):
        '''
        Parameters:
        -----------

        '''

        if callback is None:
            self.thisptr.clear_callback()
        else:
            cb = CallBack(callback, callback_with_image, data)
            self.thisptr.set_callback(callback_for_deconvolution, <void*>cb, callback_with_image)

        return self.thisptr.deconvolve(data, n1, n2, n3, v1, v2, v3)


# float
cdef class PyDeconvolveFloat:
    cdef Deconvolve[float] *thisptr # hold a C++ instance which we're wrapping

    def __cinit__(self):
        '''
        Parameters:
        -----------

        '''
        self.thisptr = new Deconvolve[float]()

    def __dealloc__(self):
        del self.thisptr

    def enable_regularization(self):
        self.thisptr.enable_regularization()

    def disable_regularization(self):
        self.thisptr.disable_regularization()

    def regularized(self):
        return self.thisptr.regularized()

    def set_snr(self, v):
        self.thisptr.set_snr(v)

    def clear_snr(self):
        self.thisptr.clear_snr()

    def set_lambda(self, v):
        self.thisptr.set_lambda(v)

    def clear_lambda(self):
        self.thisptr.clear_lambda()

    def set_max_iterations(self, v):
        self.thisptr.set_max_iterations(v)

    def clear_max_iterations(self):
        self.thisptr.clear_max_iterations()

    cpdef void set_psf(self, np.ndarray[FTYPE_t, ndim=1, mode="c"] data, size_t n1, size_t n2, size_t n3, double v1, double v2, double v3):
        '''
        Parameters:
        -----------

        '''
        self.thisptr.set_psf(data, n1, n2, n3, v1, v2, v3)

    cpdef vector[float] convolve(self, np.ndarray[FTYPE_t, ndim=1, mode="c"] data, size_t n1, size_t n2, size_t n3, double v1, double v2, double v3):
        '''
        Parameters:
        -----------

        '''
        return self.thisptr.convolve(data, n1, n2, n3, v1, v2, v3)

    cpdef vector[float] deconvolve(self, np.ndarray[FTYPE_t, ndim=1, mode="c"] data, size_t n1, size_t n2, size_t n3, double v1, double v2, double v3, callback=None, callback_with_image=False):
        '''
        Parameters:
        -----------

        '''
        if callback is None:
            self.thisptr.clear_callback()
        else:
            cb = CallBack(callback, callback_with_image, data)
            self.thisptr.set_callback(callback_for_deconvolution, <void*>cb, callback_with_image)

        return self.thisptr.deconvolve(data, n1, n2, n3, v1, v2, v3)
